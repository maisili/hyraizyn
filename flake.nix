{
  description = "hyraizyn";

  outputs = { self, kor, orbit, astra }:
  {
    strok = {
      praim = { djenyreicyn = 8; spici = "uniks"; };
      spici = "hyraizyn";
      djenyreicyn = 1;
    };

    datom = let
      astra = inputs.astra.datom;
      orbit = inputs.orbit.datom;

    in {
      inherit astra orbit;

      methydz = {
        astra = rec {
          hazTrostOf = trost: astra.pod.trost >= trost;
          izTrostyd = hazTrostOf 3;

          izEdj = astra.pod.spici == "edj";
          izSentyr = astra.pod.spici == "sentyr";

          hazSaizOf = saiz: astra.pod.saiz >= saiz;
          izMaikro = hazSaizOf 0;
          izMega = hazSaizOf 3;

          izBildyr = !izEdj && izTrostyd && (hazSaizOf 2);

          izDispatcyr = !izSentyr && izTrostyd && !izMaikro;

          izNiksKac = !izEdj && izMega;

          redjistri = {};

          nbOfBildKorz = {};

          yggdrasilKriom = {};

          meinDnsNeim = {};

        };

        orbit = {
          bildyrzKriomz = {};

          bildyrz = {};

          kacyz = {};

          dispatcyrzKriomz = {};

        };

      };
    };
  };
}
